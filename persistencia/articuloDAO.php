<?php
class articuloDAO{
    private $id;
    private $titulo;
    private $descripcion;
    private $fecha;
       
    public function articuloDAO($id = "", $titulo = "", $descripcion = "", $fecha = ""){
        $this->id = $id;
        $this->titulo = $titulo;
        $this->descripcion = $descripcion;
        $this->fecha = $fecha;
    }
    
    public function insertar(){
        return "insert into Articulos (titulo,descripcion,fecha)
                values ('" . $this->titulo . "', '" . $this->descripcion . "', '" . $this->fecha . "')";
    }
    
    public function consultarTodos(){
        return "select id,titulo,descripcion,fecha
                from Articulos";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select id,titulo,descripcion,fecha
                from Articulos
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    
    public function consultarCantidad(){
        return "select count(id)
                from Articulos";
    }
}
    
?>